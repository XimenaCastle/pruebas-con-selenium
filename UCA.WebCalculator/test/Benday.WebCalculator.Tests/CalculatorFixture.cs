﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using OpenQA.Selenium;
using OpenQA.Selenium.Chrome;
using System;
using UCA.WebCalculator.Api;

namespace UCA.WebCalculator.Tests
{
    [TestClass]
    public class CalculatorFixture
    {
        private IWebDriver driver;
        public string homeURL;

        [TestMethod]
        public void TestSite()
        {
            driver = new ChromeDriver(@"C:\Users\Ximena Silva\Desktop\Tarea 8");
            homeURL = "https://localhost:44348/calculator";

            //Instanciamos los objetos
            driver.Navigate().GoToUrl(homeURL);

            //Accesamos a las propiedades del objeto a manipular
            driver.FindElement(By.XPath("//*[@id='Value1']")).Clear();
            driver.FindElement(By.XPath("//*[@id='Value1']")).SendKeys("1");
            var a = driver.FindElement(By.XPath("//*[@id='Operator']"));
            var selectElementa = new OpenQA.Selenium.Support.UI.SelectElement(a);

            //select by value
            selectElementa.SelectByValue("Add");

            driver.FindElement(By.XPath("//*[@id='Value2']")).Clear();
            driver.FindElement(By.XPath("//*[@id='Value2']")).SendKeys("1");

            //ejecutamos las propieddes del objeto accesado
            driver.FindElement(By.XPath("/html/body/div/form/div/div/div[4]/input")).Click();

            //cerramos el driver
            driver.Close();
        }

        [TestMethod]
        public void TestSiteMultiply()
        {
            driver = new ChromeDriver(@"C:\Users\Ximena Silva\Desktop\Tarea 8");
            homeURL = "https://localhost:44348/calculator";

            //Instanciamos los objetos
            driver.Navigate().GoToUrl(homeURL);

            //Accesamos a las propiedades del objeto a manipular
            driver.FindElement(By.XPath("//*[@id='Value1']")).Clear();
            driver.FindElement(By.XPath("//*[@id='Value1']")).SendKeys("5");
            var a = driver.FindElement(By.XPath("//*[@id='Operator']"));
            var selectElementa = new OpenQA.Selenium.Support.UI.SelectElement(a);

            //select by value
            selectElementa.SelectByValue("Multiply");

            driver.FindElement(By.XPath("//*[@id='Value2']")).Clear();
            driver.FindElement(By.XPath("//*[@id='Value2']")).SendKeys("4");

            //ejecutamos las propieddes del objeto accesado
            driver.FindElement(By.XPath("/html/body/div/form/div/div/div[4]/input")).Click();

            //cerramos el driver
            
        }
        [TestMethod]
        public void TestSiteSubstract()
        {
            driver = new ChromeDriver(@"C:\Users\Ximena Silva\Desktop\Tarea 8");
            homeURL = "https://localhost:44348/calculator";

            //Instanciamos los objetos
            driver.Navigate().GoToUrl(homeURL);

            //Accesamos a las propiedades del objeto a manipular
            driver.FindElement(By.XPath("//*[@id='Value1']")).Clear();
            driver.FindElement(By.XPath("//*[@id='Value1']")).SendKeys("3");
            var a = driver.FindElement(By.XPath("//*[@id='Operator']"));
            var selectElementa = new OpenQA.Selenium.Support.UI.SelectElement(a);

            //select by value
            selectElementa.SelectByValue("Subtract");

            driver.FindElement(By.XPath("//*[@id='Value2']")).Clear();
            driver.FindElement(By.XPath("//*[@id='Value2']")).SendKeys("1");

            //ejecutamos las propieddes del objeto accesado
            driver.FindElement(By.XPath("/html/body/div/form/div/div/div[4]/input")).Click();

           }

        [TestMethod]
        public void TestSiteDivide()
        {
            driver = new ChromeDriver(@"C:\Users\Ximena Silva\Desktop\Tarea 8");
            homeURL = "https://localhost:44348/calculator";

            //Instanciamos los objetos
            driver.Navigate().GoToUrl(homeURL);

            //Accesamos a las propiedades del objeto a manipular
            driver.FindElement(By.XPath("//*[@id='Value1']")).Clear();
            driver.FindElement(By.XPath("//*[@id='Value1']")).SendKeys("10");
            var a = driver.FindElement(By.XPath("//*[@id='Operator']"));
            var selectElementa = new OpenQA.Selenium.Support.UI.SelectElement(a);

            //select by value
            selectElementa.SelectByValue("Divide");

            driver.FindElement(By.XPath("//*[@id='Value2']")).Clear();
            driver.FindElement(By.XPath("//*[@id='Value2']")).SendKeys("0");

            //ejecutamos las propieddes del objeto accesado
            driver.FindElement(By.XPath("/html/body/div/form/div/div/div[4]/input")).Click();

        }

        [TestInitialize]
        public void OnTestInitialize()
        {
            _SystemUnderTest = null;
        }
        
        private Calculator _SystemUnderTest;
        public Calculator SystemUnderTest
        {
            get
            {
                if (_SystemUnderTest == null)
                {
                    _SystemUnderTest = new Calculator();
                }

                return _SystemUnderTest;
            }
        }

        
        [TestMethod]
        public void Add()
        {
            // arrange
            double value1 = 2;
            double value2 = 3;
            double expected = 5;

            // act
            double actual = SystemUnderTest.Add(
                value1, value2);

            // assert
            Assert.AreEqual<double>(expected, actual, "Wrong result.");
        }


       


        [TestMethod]
        public void Subtract()
        {
            // arrange
            double value1 = 6;
            double value2 = 2;
            double expected = 4;

            // act
            double actual = SystemUnderTest.Subtract(
                value1, value2);

            // assert
            Assert.AreEqual<double>(expected, actual, "Wrong result.");
        }

        [TestMethod]
        public void Multiply()
        {
            // arrange
            double value1 = 6;
            double value2 = 2;
            double expected = 12;

            // act
            double actual = SystemUnderTest.Multiply(
                value1, value2);

            // assert
            Assert.AreEqual<double>(expected, actual, "Wrong result.");
        }

        [TestMethod]
        public void Divide()
        {
            // arrange
            double value1 = 6;
            double value2 = 2;
            double expected = 3;

            // act
            double actual = SystemUnderTest.Divide(
                value1, value2);

            // assert
            Assert.AreEqual<double>(expected, actual, "Wrong result.");
        }

        [TestMethod]
        [ExpectedException(typeof(InvalidOperationException))]
        public void DivideByZeroThrowsException()
        {
            // arrange
            double value1 = 6;
            double value2 = 0;

            // act
            double actual = SystemUnderTest.Divide(
                value1, value2);
        }
    }
}
