using Microsoft.AspNetCore.Mvc;
using System;
using UCA.WebCalculator.Api;
using OpenQA.Selenium;
using OpenQA.Selenium.Chrome;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace UCA.WebCalculator.Tests
{
    public class UnitTestUtility
    {
        public static T GetModel<T>(IActionResult actionResult) where T : class
        {
            var asViewResult = actionResult as ViewResult;

            return asViewResult.Model as T;
        }
            

    }
}